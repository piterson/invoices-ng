import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { HttpModule } from '@angular/http';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SmartHttpInterceptor } from './http-interceptor';
import { AppComponent } from './app.component';
import { CustomerItemComponent } from './components/customer-componets/customer-item/customer-item.component';
import { CustomersComponent } from './components/customer-componets/customers/customers.component';

import { CustomersService } from './services/customers.service';
import { ProductsService } from './services/products.service';
import { InvoicesService } from './services/invoices.service';
import { InvoiceItemsService } from './services/invoice-items.service';
import { PreloaderService } from './services/preloader.service';
import { CustomerDeailComponent } from './components/customer-componets/customer-deail/customer-deail.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { EditedCreatedComponent } from './components/common/edited-created/edited-created.component';
import { ProductItemComponent } from './components/product-components/product-item/product-item.component';
import { ProductsComponent } from './components/product-components/products/products.component';
import { ProductDetailComponent } from './components/product-components/product-detail/product-detail.component';
import { InvoiceItemComponent } from './components/invoice-components/invoice-item/invoice-item.component';
import { InvoicesComponent } from './components/invoice-components/invoices/invoices.component';
import { InvoiceDetailComponent } from './components/invoice-components/invoice-detail/invoice-detail.component';
import { InvoiceItemDetailComponent } from './components/invoice-components/invoice-item-detail/invoice-item-detail.component';


const appRoutes: Routes = [
  // { path: 'crisis-center', component: CrisisListComponent },
  { path: 'customer/:id',      component: CustomerDeailComponent },
  { path: 'customer',      component: CustomerDeailComponent },
  {
    path: 'customers',
    component: CustomersComponent,
    data: { title: 'Customers List' }
  },
  { path: 'product/:id',      component: ProductDetailComponent },
  { path: 'product',      component: ProductDetailComponent },
  {
    path: 'products',
    component: ProductsComponent,
    data: { title: 'Products List' }
  },
  { path: 'invoice/:id',      component: InvoiceDetailComponent },
  { path: 'invoice',      component: InvoiceDetailComponent },
  {
    path: 'invoices',
    component: InvoicesComponent,
    data: { title: 'Invoices List' }
  },
  { path: '',
    redirectTo: '/invoices',
    pathMatch: 'full'
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CustomerItemComponent,
    CustomersComponent,
    CustomerDeailComponent,
    NotFoundComponent,
    EditedCreatedComponent,
    ProductItemComponent,
    ProductsComponent,
    ProductDetailComponent,
    InvoiceItemComponent,
    InvoicesComponent,
    InvoiceDetailComponent,
    InvoiceItemDetailComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    CustomersService,
    ProductsService,
    InvoicesService,
    InvoiceItemsService,
    PreloaderService,
    {
        provide: HTTP_INTERCEPTORS,
        useClass: SmartHttpInterceptor,
        multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
