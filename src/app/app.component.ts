import { Component, OnInit } from '@angular/core';
import { PreloaderService } from './services/preloader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  showProgress = false;

  constructor(private preloader: PreloaderService) {
  }

  ngOnInit() {
    this.preloader.subscribe((value: boolean) => {
      this.showProgress = value;
    });
  }
}
