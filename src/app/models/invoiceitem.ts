import {Generic} from './generic';

export class InvoiceItem extends Generic {
  invoice_id: number;
  product_id: number;
  quantity: number;
}
