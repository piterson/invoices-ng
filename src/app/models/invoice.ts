import {Generic} from './generic';

export class Invoice extends Generic {
  customer_id: number;
  discount: number;
  total: number;
}
