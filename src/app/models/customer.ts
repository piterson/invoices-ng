import {Generic} from './generic';

export class Customer extends Generic {
  name: string;
  address: string;
  phone: string;
}
