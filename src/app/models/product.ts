import {Generic} from './generic';

export class Product extends Generic {
  name: string;
  price: number;
}
