import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import { environment } from '../environments/environment';
import { PreloaderService } from './services/preloader.service';

@Injectable()
export class SmartHttpInterceptor implements HttpInterceptor {
    constructor(private preloaderService: PreloaderService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        console.log('intercepted request ... ', req.headers);
        this.preloaderService.showPreloader();

        // Clone the request to add the new header.
        const authReq = req.clone(
            {
                // withCredentials: true,
                url: environment.apiHost + req.url,
                headers: req.headers // .set("headerName", "headerValue")
            }
        );

        console.log('Sending request with new header now ...');

        // send the newly created request
        return next.handle(authReq)
            .do(event => {
              if (event instanceof HttpResponse) {
                this.preloaderService.hidePreloader();
              }
//                if(o.error){
//                  console.log("Error Received", o.error);
//                }else{
//                  console.log("Received", o.result);
//                }
            })
            .catch((error, caught) => {
              this.preloaderService.hidePreloader();
                // intercept the respons error and displace it to the console
                console.log('Error Occurred');
                console.log(error);
                // return the error to the method that called it
                return Observable.throw(error);
            }) as any;
    }
}
