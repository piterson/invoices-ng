import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditedCreatedComponent } from './edited-created.component';

describe('EditedCreatedComponent', () => {
  let component: EditedCreatedComponent;
  let fixture: ComponentFixture<EditedCreatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditedCreatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditedCreatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
