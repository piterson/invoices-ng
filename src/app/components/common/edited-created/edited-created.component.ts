import { Component, Input } from '@angular/core';

import { Generic } from '../../../models/generic';

@Component({
  selector: 'app-edited-created',
  templateUrl: './edited-created.component.html',
  styleUrls: ['./edited-created.component.css']
})
export class EditedCreatedComponent {
  @Input() data: Generic;

  constructor() { }

}
