import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Product } from '../../../models/product';
import { ProductsService } from '../../../services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[];
  error: any;

  constructor(private productService: ProductsService,
  private router: Router) { }

  getProducts() {
    this.productService.getAll()
      .then(products => this.products = products)
      .catch(error => this.error = error);
  }

  ngOnInit() {
    this.getProducts();
  }

  onSelect(prod: Product) {
  }

  onCreate() {
    this.router.navigate(['/product']);
  }

  deleteProduct(prod: Product) {
    this.productService.delete(prod)
      .then(() => this.products.splice(this.products.indexOf(prod), 1));
  }
}
