import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

import { Product } from '../../../models/product';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {
  @Input() product: Product;
  @Output() onDelete = new EventEmitter<Product>();

  constructor() { }

  ngOnInit() {
  }

  deleteClick() {
    this.onDelete.emit(this.product);
  }
}
