import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Product } from '../../../models/product';
import { ProductsService } from '../../../services/products.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  product: Product;

  constructor(private productsService: ProductsService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      if (params['id'] !== undefined) {
        const id = +params['id'];
        // this.navigated = true;
        this.productsService.getById(id)
            .then(prod => this.product = prod);
      } else {
        // this.navigated = false;
        this.product = new Product();
      }
    });
  }

  onSave() {
    this.productsService.save(this.product)
      .then(prod => this.product = prod);
  }

  onCancel() {
    this.router.navigate(['/products']);
  }

}
