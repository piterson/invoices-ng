import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Customer } from '../../../models/customer';
import { CustomersService } from '../../../services/customers.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  customers: Customer[];
  error: any;

  constructor(private customerService: CustomersService,
  private router: Router) { }

  getCustomers() {
    this.customerService.getAll()
      .then(customers => this.customers = customers)
      .catch(error => this.error = error);
  }

  ngOnInit() {
    this.getCustomers();
  }

  onSelect(cust: Customer) {
  }

  onCreate() {
    this.router.navigate(['/customer']);
  }

  deleteCustomer(cust: Customer) {
    this.customerService.delete(cust)
      .then(() => this.customers.splice(this.customers.indexOf(cust), 1));
  }

}
