import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Customer } from '../../../models/customer';

@Component({
  selector: 'app-customer-item',
  templateUrl: './customer-item.component.html',
  styleUrls: ['./customer-item.component.css']
})
export class CustomerItemComponent implements OnInit {
  @Input() customer: Customer;
  @Output() onDelete = new EventEmitter<Customer>();

  constructor() { }

  ngOnInit() {
  }

  deleteClick() {
    this.onDelete.emit(this.customer);
  }
}
