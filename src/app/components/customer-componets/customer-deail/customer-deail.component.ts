import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Customer } from '../../../models/customer';
import { CustomersService } from '../../../services/customers.service';

@Component({
  selector: 'app-customer-deail',
  templateUrl: './customer-deail.component.html',
  styleUrls: ['./customer-deail.component.css']
})
export class CustomerDeailComponent implements OnInit {
  customer: Customer;

  constructor(private customersService: CustomersService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      if (params['id'] !== undefined) {
        const id = +params['id'];
        // this.navigated = true;
        this.customersService.getById(id)
            .then(cust => this.customer = cust);
      } else {
        // this.navigated = false;
        this.customer = new Customer();
      }
    });
  }

  onSave() {
    this.customersService.save(this.customer)
      .then(cust => this.customer = cust)
      .then(() => console.log(this.customer.updatedAt));
  }

  onCancel() {
    this.router.navigate(['/customers']);
  }

}
