import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerDeailComponent } from './customer-deail.component';

describe('CustomerDeailComponent', () => {
  let component: CustomerDeailComponent;
  let fixture: ComponentFixture<CustomerDeailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerDeailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerDeailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
