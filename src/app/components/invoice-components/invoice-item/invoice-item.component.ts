import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Invoice } from '../../../models/invoice';

@Component({
  selector: 'app-invoice-item',
  templateUrl: './invoice-item.component.html',
  styleUrls: ['./invoice-item.component.css']
})
export class InvoiceItemComponent implements OnInit {
  @Input() invoice: Invoice;
  @Output() onDelete = new EventEmitter<Invoice>();

  constructor() { }

  ngOnInit() {
  }

  deleteClick() {
    this.onDelete.emit(this.invoice);
  }

}
