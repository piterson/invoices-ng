import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { InvoiceItem } from '../../../models/invoiceitem';
import { Product } from '../../../models/product';
import { ProductsService } from '../../../services/products.service';

@Component({
  selector: 'app-invoice-item-detail',
  templateUrl: './invoice-item-detail.component.html',
  styleUrls: ['./invoice-item-detail.component.css']
})
export class InvoiceItemDetailComponent implements OnInit {
  @Input() invoiceItem: InvoiceItem;
  @Output() onDelete = new EventEmitter<InvoiceItem>();
  @Output() onChange = new EventEmitter();
  products: Product[];
  selectedProduct: Product;

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.productsService.getCache()
      .then(items => this.products = items)
      .then(() => {
        this.selectedProduct = this.products.find(itm => itm.id === this.invoiceItem.id);
        this.emitChange();
      });
  }

  deleteClick() {
    this.onDelete.emit(this.invoiceItem);
  }

  emitChange() {
    this.onChange.emit();
  }

  get total() {
    if (!this.selectedProduct || !this.invoiceItem || !this.invoiceItem.quantity) {
      return 0;
    }
    return Math.round(100 * this.selectedProduct.price * this.invoiceItem.quantity) / 100;
  }

  onProductSelected(e: any) {
    this.selectedProduct = this.products[e.target.selectedIndex];
    this.emitChange();
  }

}
