import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceItemDetailComponent } from './invoice-item-detail.component';

describe('InvoiceItemDetailComponent', () => {
  let component: InvoiceItemDetailComponent;
  let fixture: ComponentFixture<InvoiceItemDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceItemDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceItemDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
