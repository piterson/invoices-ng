import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Invoice } from '../../../models/invoice';
import { InvoicesService } from '../../../services/invoices.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {
  invoices: Invoice[];
  error: any;

  constructor(private invoiceService: InvoicesService,
  private router: Router) { }

  getInvoices() {
    this.invoiceService.getAll()
      .then(customers => this.invoices = customers)
      .catch(error => this.error = error);
  }

  ngOnInit() {
    this.getInvoices();
  }

  onCreate() {
    this.router.navigate(['/invoice']);
  }

  deleteInvoice(invc: Invoice) {
    this.invoiceService.delete(invc)
      .then(() => this.invoices.splice(this.invoices.indexOf(invc), 1));
  }

}
