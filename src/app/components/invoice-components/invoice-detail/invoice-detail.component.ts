import { Component, OnInit, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Invoice } from '../../../models/invoice';
import { InvoiceItem } from '../../../models/invoiceitem';
import { Customer } from '../../../models/customer';
import { InvoicesService } from '../../../services/invoices.service';
import { InvoiceItemsService } from '../../../services/invoice-items.service';
import { CustomersService } from '../../../services/customers.service';
import { InvoiceItemDetailComponent } from '../invoice-item-detail/invoice-item-detail.component';

@Component({
  selector: 'app-invoice-detail',
  templateUrl: './invoice-detail.component.html',
  styleUrls: ['./invoice-detail.component.css']
})
export class InvoiceDetailComponent implements OnInit, AfterViewInit {
  invoice: Invoice;
  invoiceItems: InvoiceItem[];
  customers: Customer[];

  constructor(private invoicesService: InvoicesService,
    private invoiceItemsService: InvoiceItemsService,
    private customersService: CustomersService,
    private route: ActivatedRoute,
    private router: Router) { }
    @ViewChildren(InvoiceItemDetailComponent) itemChildren: QueryList<InvoiceItemDetailComponent>;
    protected subTotal = 0;

  ngOnInit() {
    this.customersService.getAll()
      .then(customers => this.customers = customers);
    this.route.params.forEach((params: Params) => {
      if (params['id'] !== undefined) {
        const id = +params['id'];
        // this.navigated = true;
        this.invoicesService.getById(id)
          .then(invc => {
            this.invoice = invc;
            return this.invoiceItemsService.getEntity(invc.id).getAll();
          })
          .then(items => this.invoiceItems = items);
      } else {
        // this.navigated = false;
        this.invoice = new Invoice();
        this.invoiceItems = [new InvoiceItem()];
      }
    });
  }

  ngAfterViewInit() {
    this.calculateInvoiceSubTotal();
    // this.itemChildren.changes.subscribe((r) => { this.calculateInvoiceSubTotal(); });
  }

  calculateInvoiceSubTotal() {
    setTimeout(() => {
      this.subTotal = this.itemChildren
        .map(p => p.total)
        .reduce((r, v) => r += v, 0);
      this.calculateTotal();
    }, 0);
  }

  calculateTotal() {
    if (!this.invoice) {
      return;
    }
    this.invoice.total = this.invoice.discount ? (1 - this.invoice.discount / 100) * this.subTotal : this.subTotal;
    this.invoice.total = Math.round(100 * this.invoice.total) / 100;
  }

  onSave() {
    this.invoicesService.save(this.invoice)
      .then(invc => {
        this.invoice = invc;
        const srv: InvoiceItemsService = this.invoiceItemsService.getEntity(invc.id);
        return Promise.all(this.invoiceItems.map(itm => (itm.invoice_id = invc.id, srv.save(itm))));
      })
      .then(items => this.invoiceItems = items);
  }

  addItem() {
    this.invoiceItems.push(new InvoiceItem());
  }

  deleteItem(item: InvoiceItem) {
    Promise.resolve()
      .then(() => {
        if (item.id) {
          return this.invoiceItemsService.getEntity(this.invoice.id).delete(item);
        }
      })
      .then(() => this.invoiceItems.splice(this.invoiceItems.indexOf(item), 1));
  }

  onCancel() {
    this.router.navigate(['/invoices']);
  }

}
