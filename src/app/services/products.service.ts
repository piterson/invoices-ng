import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Product } from '../models/product';
import { CRUDService } from './crud.service';

@Injectable()
export class ProductsService extends CRUDService<Product> {
  private cache: Product[];

  constructor(protected http: HttpClient) {
    super(http);
  }

  protected get entityUrl(): string {
    return 'api/products';
  }

  getCache(): Promise<Array<Product>> {
    return Promise.resolve()
      .then(() => {
          if (!this.cache) {
            return this.getAll();
          }
        return this.cache;
      })
      .then(products => (this.cache = products, this.cache));
  }

}
