import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Customer } from '../models/customer';
import { CRUDService } from './crud.service';

@Injectable()
export class CustomersService extends CRUDService<Customer> {

  constructor(protected http: HttpClient) {
    super(http);
  }

  protected get entityUrl(): string {
    return 'api/customers';
  }
}
