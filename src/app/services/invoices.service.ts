import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Invoice } from '../models/invoice';
import { CRUDService } from './crud.service';

@Injectable()
export class InvoicesService extends CRUDService<Invoice> {

  constructor(protected http: HttpClient) {
    super(http);
  }

  protected get entityUrl(): string {
    return 'api/invoices';
  }

}
