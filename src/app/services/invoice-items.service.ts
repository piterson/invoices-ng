import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { InvoiceItem } from '../models/invoiceitem';
import { CRUDService } from './crud.service';

@Injectable()
export class InvoiceItemsService extends CRUDService<InvoiceItem> {
  private invoiceId: number;

  constructor(protected http: HttpClient) {
    super(http);
  }

  private setInvoice(invoiceId: number) {
    this.invoiceId = invoiceId;
    return this;
  }

  protected get entityUrl(): string {
    if (isNaN(this.invoiceId)) {
      throw new Error('You can\'t call this in primary service');
    }
    return `api/invoices/${this.invoiceId}/items`;
  }

  getEntity(invoiceId: number): InvoiceItemsService {
    return new InvoiceItemsService(this.http).setInvoice(invoiceId);
  }

}
