import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';

import { Generic } from '../models/generic';

export class CRUDService<T extends Generic> {
  private headers: HttpHeaders;

  constructor(protected http: HttpClient) {
    this.headers = new HttpHeaders().set('Content-Type', 'application/json');
  }

  protected get entityUrl(): string {
    throw new Error('this method should be implemented');
  }

  getAll(): Promise<Array<T>> {
    return this.http
      .get<T[]>(this.entityUrl)
      .toPromise()
      .catch(this.handleError);
  }

  getById(id: number): Promise<T> {
    const url = `${this.entityUrl}/${id}`;

    return this.http
      .get<T>(url)
      .toPromise()
      .catch(this.handleError);
//      .then(heroes => heroes.find(hero => hero.id === id));
  }

  save(cust: T): Promise<T> {
    if (cust.id) {
      return this.put(cust);
    }
    return this.post(cust);
  }

  delete(cust: T): Promise<any> {
    const url = `${this.entityUrl}/${cust.id}`;

    return this.http
      .delete(url, { headers: this.headers })
      .toPromise()
      .catch(this.handleError);
  }

  // Add new Hero
  private post(cust: T): Promise<T> {

    return this.http
      .post<T>(this.entityUrl, JSON.stringify(cust), { headers: this.headers })
      .toPromise()
      .catch(this.handleError);
  }

  // Update existing Hero
  private put(cust: T): Promise<T> {

    const url = `${this.entityUrl}/${cust.id}`;

    return this.http
      .put<T>(url, JSON.stringify(cust), { headers: this.headers })
      .toPromise()
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
