import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

@Injectable()
export class PreloaderService {

  private preloaderStatus: Observable<boolean>;
  private preloadObserver: Observer<boolean>;
  private showCount = 0;

  constructor() {
    this.preloaderStatus = new Observable((observer: Observer<boolean>) => {
      this.preloadObserver = observer;
    });
  }

  subscribe(f: Function) {
    this.preloaderStatus.subscribe((value: boolean) => {
      f.call(null, value);
    });
  }

  showPreloader() {
    this.showCount ++;
    this.preloadObserver.next(true);
  }

  hidePreloader() {
    this.showCount --;
    if (this.showCount <= 0) {
      this.showCount = 0;
      this.preloadObserver.next(false);
    }
  }

  popError() {
    console.log('popError');
  }
}
